
# soyboy slayin dotfiles

### Reasons to use these epic dotfiles:

- Lightweight
- Slick
- Very Fast

# Install procedures

### neovim

Install the packages for neovim.

  

- arch: # `pacman -S neovim python-pynvim`
- gentoo # `emerge -aq app-editors/neovim dev-python/pynvim`


Install vim-plug (vim package manager ig)

-`mkdir ~/.config/nvim`  
-`mkdir ~/.config/nvim/plugged`

-`sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/ junegunn/vim-plug/master/plug.vim'`

- Copy the config from my gitlab

- obtain this file: https://gitlab.com/succulent99/dotfiles/-/raw/master/.config/nvim/init.vim?inline=false  
- put it in `~/.config/nvim/init.vim`

Install packages

- open nvim and use the command `:PlugInstall`

### XMonad

Install the packages for XMonad.

- arch: # `package -S xmonad xmonad-contrib`
- gentoo: # `emerge -aq x11-wm/xmonad x11-wm/xmonad-contrib`

Copy the config files for XMonad

- `mkdir ~/.xmonad/`
- put https://gitlab.com/succulent99/dotfiles/-/raw/master/.xmonad/xmonad.hs?inline=false into `~/.xmonad/`

Add `xmonad to your ~/.xinitrc`
