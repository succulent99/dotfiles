  -- Base
import XMonad
import System.Directory
import System.IO
import System.Exit 
import qualified XMonad.StackSet as W

    -- Actions
import XMonad.Actions.CopyWindow
import XMonad.Actions.CycleWS
import XMonad.Actions.GridSelect
import XMonad.Actions.MouseResize
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves
import XMonad.Actions.WindowGo
import XMonad.Actions.WithAll
import qualified XMonad.Actions.Search as S

    -- Data
import Data.Char
import Data.Maybe
import Data.Monoid
import Data.Maybe
import Data.Tree
import Data.Monoid
import qualified Data.Map as M

    -- Hooks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops  -- for some fullscreen events, also for xcomposite in obs.
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.ServerMode
import XMonad.Hooks.SetWMName
import XMonad.Hooks.WorkspaceHistory

    -- Layouts
import XMonad.Layout.Accordion
import XMonad.Layout.GridVariants
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns
import XMonad.Layout.BinarySpacePartition

    -- Layouts modifiers
import XMonad.Layout.Reflect
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowArranger 
import XMonad.Layout.WindowNavigation
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

   -- Utilities
import XMonad.Util.Dmenu
import XMonad.Util.EZConfig
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run
import XMonad.Util.SpawnOnce
import XMonad.Util.Font
import Data.List
import System.Directory
import System.Environment
import System.Exit
import System.IO

getConfigFilePath f =
  getHomeDirectory >>= \hd -> return $ hd ++ "/" ++ f

getWalColors = do
  file <- getConfigFilePath ".cache/wal/colors"
  contents <- readFile file
  let colors = lines contents
  return (colors ++ (replicate (16 - length colors) "#000000"))

myFont :: String
myFont = "xft:mononoki Nerd Font Mono:regular:size=9:antialias=true:hinting=true"

myTerminal      = "kitty"
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True
myClickJustFocuses :: Bool
myClickJustFocuses = False
myBorderWidth   = 3
myModMask       = mod1Mask

xmobarEscape = concatMap doubleLts
  where doubleLts '<' = "<<"
        doubleLts x    = [x]

myWorkspaces            = clickable . (map xmobarEscape) $ [" dev ", " www ", " sys ", " game ", " qemu ", " chat ", " mus ", " vid ", " gimp "]
    where
         clickable l = [ "<action=xdotool key alt+" ++ show (n) ++ ">" ++ ws ++ "</action>" |
                            (i,ws) <- zip [1..9] l,                                        
                            let n = i ]
-- myWorkspaces = map xmobarEscape $ ["1", "2", "3", "4", "5", "6", "7", "8", "9"]

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset


myNormalBorderColor  = "#282828"
myFocusedBorderColor = "#ebdbb2"
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    [ ((modm .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf)
    , ((modm,               xK_Return), spawn "rofi -show drun -show-icons")
    , ((modm .|. shiftMask, xK_c     ), kill)
    , ((modm,               xK_space ), sendMessage NextLayout)
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)
    , ((modm,               xK_Tab   ), windows W.focusUp)
    , ((modm,               xK_s     ), withFocused $ windows . W.sink)
    , ((modm .|. shiftMask, xK_s     ), spawn "flameshot gui")
    , ((modm,               xK_p     ), spawn "xwininfo | xmessage -file -")
    , ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))
    , ((modm              , xK_q     ), spawn "xmonad --recompile; xmonad --restart")
    ]
    ++

    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++

    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]


--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))
    ]

--Makes setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- Below is a variation of the above except no borders are applied
-- if fewer than two windows. So a single window has no gaps.
mySpacing' :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

-- Defining a bunch of layouts, many that I don't use.
-- limitWindows n sets maximum number of windows displayed for layout.
-- mySpacing n sets the gap size around the windows.

bsp      = renamed [Replace "bsp"]
           $ reflectHoriz 
           $ reflectVert
           $ mySpacing 20
           $ emptyBSP 
monocle  = renamed [Replace "monocle"]
           $ smartBorders
           $ windowNavigation
           $ mySpacing 20
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 20 Full
floats   = renamed [Replace "floats"]
           $ smartBorders
           $ limitWindows 20 simplestFloat

--tabs     = renamed [Replace "tabs"]
--           $ mySpacing 20
           -- I cannot add spacing to this layout because it will
           -- add spacing between window and tabs which looks bad.
--           $ tabbed shrinkText myTabTheme

-- setting colors for tabs layout and tabs sublayout.
myTabTheme = def { fontName            = myFont
                 , activeColor         = "#46d9ff"
                 , inactiveColor       = "#313846"
                 , activeBorderColor   = "#46d9ff"
                 , inactiveBorderColor = "#282c34"
                 , activeTextColor     = "#282c34"
                 , inactiveTextColor   = "#d0d0d0"
                 }

-- The layout hook
myLayoutHook = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout =     withBorder myBorderWidth bsp 
                                 -- ||| magnify
                                 -- ||| floats
                                 ||| monocle
                                 -- ||| grid
                                 -- ||| spirals
                                 -- ||| threeCol
                                 -- ||| threeRow
                                 -- ||| tallAccordion
                                 -- ||| wideAccordion


myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore ]

myStartupHook = do
        spawnOnce "xsetroot -cursor_name left_ptr"
        setWMName "LG3D"


--main = do
    --xmproc <- spawnPipe "~/bin/polybar-init.sh"
    --xmproc <- spawnPipe "polybar example"
    --xmproc <- spawnPipe "xmobar ~/.config/xmobar/xmobar.hs"
    --xmproc <- spawnPipe "polybar example"
    --trayer <- spawnPipe "trayer --edge top --align right --widthtype request --expand true --SetDockType true --SetPartialStrut true --transparent true --alpha 0 --tint 0x282828 --expand true --heighttype pixel --height 20 --monitor 1 --padding 1"
main = do
    colors <- getWalColors
    xmproc <- spawnPipe "xmobar $HOME/.xmonad/xmobar.hs"

    xmonad $ docks $ ewmh def {
        handleEventHook    = handleEventHook def <+> fullscreenEventHook,
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = colors!!10,
        focusedBorderColor = colors!!12,

        keys               = myKeys,
        mouseBindings      = myMouseBindings,
        logHook = dynamicLogWithPP $ namedScratchpadFilterOutWorkspacePP $ xmobarPP
              -- the following variables beginning with 'pp' are settings for xmobar.
              { ppOutput          = hPutStrLn xmproc                             -- xmobar on monitor 1
              , ppCurrent         = xmobarColor "#98be65" "" . wrap "[" "]"           -- Current workspace
              , ppVisible         = xmobarColor "#98be65" ""                -- Visible but not current workspace
              , ppHidden          = xmobarColor "#82AAFF" "" . wrap "*" ""
              , ppHiddenNoWindows = xmobarColor "#c792ea" ""                   -- Hidden workspaces (no windows)
              , ppTitle           = xmobarColor "#b3afc2" "" . shorten 60               -- Title of active window
              , ppSep             =  "<fc=#666666> <fn=1>|</fn> </fc>"                    -- Separator character
              , ppUrgent          = xmobarColor "#C45500" "" . wrap "!" "!"            -- Urgent workspace
              , ppExtras          = [windowCount]                                     -- # of windows current workspace
              , ppOrder           = \(ws:l:t:ex) -> [ws,l]++ex++[t]                    -- order of things in xmobar
              },
        layoutHook         = myLayoutHook,
        manageHook         = myManageHook,
        startupHook        = myStartupHook
    }

