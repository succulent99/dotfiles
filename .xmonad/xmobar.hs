
Config { font    = "xft:Ubuntu:weight=bold:pixelsize=11:antialias=true:hinting=true"
       , additionalFonts = [ "xft:Mononoki Nerd Font:pixelsize=11:antialias=true:hinting=true"
                           , "xft:Font Awesome 5 Free Solid:pixelsize=12"
                           , "xft:Font Awesome 5 Brands:pixelsize=12"
                           ]
       , bgColor = "#0D0F10"
       , fgColor = "#ff6c6b"
       , position = Static { xpos = 0 , ypos = 0, width = 2560, height = 24 }
       , border =       BottomB
       , borderColor =  "#975E6D"
       , lowerOnStart = True
       , hideOnStart = False
       , allDesktops = True
       , persistent = True
       , commands = [
                    -- Time and date
                      Run Date "<fn=2>\xf017</fn>  %b %d %Y - (%H:%M) " "date" 50
                    , Run Cpu ["-t", "<fn=2>\xf108</fn>  cpu: (<total>%)","-H","50","--high","red"] 20
                    , Run Memory ["-t", "<fn=2>\xf233</fn>  mem: <used>M (<usedratio>%)"] 20
                    , Run DiskU [("/", "<fn=2>\xf0c7</fn>  hdd: <free> free")] [] 60
                    , Run Com "uname" ["-r"] "" 3600
                    , Run UnsafeStdinReader
                    ]
       , sepChar = "%"
       , alignSep = "}{"
       , template = " <icon=/home/lucas/.xmonad/xpm/haskell_20.xpm/>  <fc=#666666>|</fc> %UnsafeStdinReader% }{  <fc=#666666>|</fc>  <fc=#b3afc2><fn=3></fn>  <action=`kitty -e htop`>%uname%</action> </fc> <fc=#666666>|</fc>  <fc=#ecbe7b> <action=`kitty -e htop`>%cpu%</action> </fc> <fc=#666666>|</fc>  <fc=#ff6c6b> %memory% </fc> <fc=#666666>|</fc>  <fc=#51afef> <action=`kitty -e htop`>%disku%</action> </fc> <fc=#666666>|</fc> <fc=#46d9ff> %date% </fc><fc=#666666><fn=1>|</fn></fc>"
       }

