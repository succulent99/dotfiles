config.load_autoconfig(False)

config.source('qutewal/qutewal.py')

#c.colors.webpage.darkmode.enabled = True
c.statusbar.show = "in-mode"
c.tabs.show = "multiple"
c.tabs.padding = {"bottom": 5, "left": 5, "right": 5, "top": 5}

c.downloads.location.directory = "~/downloads/"
c.url.default_page = "~/.config/qutebrowser/startpage.html"
config.set('url.start_pages','~/.config/qutebrowser/startpage.html')
