vim.g.nvchad_theme = "tomorrow-night"

local base16
if
    not pcall(
        function()
            base16 = require "base16"
        end
    )
 then
    return false
else
    base16(base16.themes["tomorrow-night"], true)
    return true
end
