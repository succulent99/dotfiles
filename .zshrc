colorscript -r

export ZSH="/home/lucas/.oh-my-zsh"
ZSH_THEME="robbyrussell"
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )
# CASE_SENSITIVE="true"
# HYPHEN_INSENSITIVE="true"
# DISABLE_AUTO_UPDATE="true"
# DISABLE_UPDATE_PROMPT="true"
# export UPDATE_ZSH_DAYS=13
# DISABLE_MAGIC_FUNCTIONS="true"
# DISABLE_LS_COLORS="true"
# DISABLE_AUTO_TITLE="true"
# ENABLE_CORRECTION="true"
# COMPLETION_WAITING_DOTS="true"
# DISABLE_UNTRACKED_FILES_DIRTY="true"
# HIST_STAMPS="mm/dd/yyyy"
# ZSH_CUSTOM=/path/to/new-custom-folder
plugins=(git zsh-autosuggestions zsh-syntax-highlighting)

source $ZSH/oh-my-zsh.sh

EDITOR='nvim'
VISUAL='nvim'
TERMINAL='kitty'



export LANG=en_US.UTF-8
export PATH="$HOME/bin:$HOME/.config/rofi/bin:$HOME/.cargo/bin:$HOME/.local/bin:$PATH"

alias emerge-log='sudo tail -f /var/log/emerge.log'
alias temps='sensors'

if [ -f /etc/bash.command-not-found ]; then
    . /etc/bash.command-not-found
fi


eval "$(starship init zsh)"

SPACESHIP_GIT_STATUS_SHOW='false'
